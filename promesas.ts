let promise1 = new Promise(function(resolve, reject) {

    setTimeout( () => {
        console.log("Promesa Terminada");

        //Termina correctamente
        resolve();

        //Termina con error
        //reject();
    }, 1500);
});

promise1.then( function() {
    console.log("Ejecutarme cuando se termine bien");
}, function() {
    console.error("Ejecutar si todo sale mal");
});