"use strict";
class Avenger {
    constructor(nombre, equipo, nombreReal) {
        this.puedePelear = false;
        this.peleasGanadas = 0;
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
        console.log("Se ejecuto el Constructor");
    }
}
let antman = new Avenger("Antman", "Cap", "Scoot Lang");
console.log(antman);
