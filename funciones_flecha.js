"use strict";
let miFuncion = function (a) {
    return a;
};
let miFuncionF = (a) => a;
let miFuncion2 = function (a, b) {
    return a + b;
};
let miFuncion2F = (a, b) => a + b;
let miFuncion3 = function (nombre) {
    nombre = nombre.toUpperCase();
    return nombre;
};
let miFuncion3F = (nombre) => {
    nombre = nombre.toUpperCase();
    return nombre;
};
let hulk = {
    nombre: "Hulk",
    smash() {
        setTimeout(() => console.log(this.nombre + " smash!"), 1500);
    }
};
hulk.smash();
console.log(miFuncion2(1, 2));
console.log(miFuncion2F(1, 4));
