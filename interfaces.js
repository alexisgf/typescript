"use strict";
function enviarMision(xmen) {
    console.log("Enviando a: " + xmen.nombre);
}
function enviarCuartel(xmen) {
    console.log("Enviando al Cuartel a: " + xmen.nombre);
}
let wolverin = {
    nombre: "Wolverine",
    poder: "Regeneracion"
};
enviarMision(wolverin);
enviarCuartel(wolverin);
