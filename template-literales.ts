
function getNombre() {
    return "Alexis Manuel";
}

let nomb:string = "Juan";
let apellido:string = "Perez";
let edad:number = 32;

//let texto = "Hola " + nomb + " " + apellido + "(" + edad +")";

let texto = `Hola, ${ nomb } ${apellido} (${ edad })`;

let texto2:string = `${ getNombre() }`;

console.log(texto2);