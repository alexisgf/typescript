//let y const
let nombr:string =  "Ricardo Tapia";
let edades:number = 23;
const PERSONA = {
    nombre:nombr,
    edad: edades
}


//funcion flecha
let resultado = (a:number, b:number) => (a + b)*2;

console.log(resultado(1,2));

//Interaz
interface Batman {
    nombre:string,
    artesMarciales:string[];
}

let batman:Batman =  {
    nombre: "Bruno Diaz",
    artesMarciales: ["Karate", "Aikido", "Wing Chun", "Jiu-Jitsu"]
}

//Funcion con parametros
function getAvenger(nombre:string, poder?:string, arma:string = "Arco"){
    let mensaje;
    if(poder) {
        mensaje = `${ nombre } tiene el poder de: ${ poder } y un arma: ${ arma }`;
    }
    else {
        mensaje = `${ nombre } tiene un ${ arma }`;
    }

    console.log(mensaje);
}

getAvenger("Alexis", "Volar", "Pistola");



//Clase

class Rectangulo {
    public base:number;
    public altura:number;

    constructor(base:number, altura:number) {
        this.base = base;
        this.altura = altura;
    }

    calcularArea() {
        return this.base*this.altura;
    }
}

let rectangulo:Rectangulo = new Rectangulo(2,5);

console.log(rectangulo.calcularArea());
