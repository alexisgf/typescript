"use strict";
//funcion flecha
let resultado = (a, b) => (a + b) * 2;
console.log(resultado(1, 2));
//Funcion con parametros
function getAvenger(nombre, poder, arma = "Arco") {
    let mensaje;
    if (poder) {
        mensaje = `${nombre} tiene el poder de: ${poder} y un arma: ${arma}`;
    }
    else {
        mensaje = `${nombre} tiene un ${arma}`;
    }
    console.log(mensaje);
}
getAvenger("Alexis", "Volar", "Pistola");
//Clase
class Rectangulo {
    constructor(base, altura) {
        this.base = base;
        this.altura = altura;
    }
    calcularArea() {
        return this.base * this.altura;
    }
}
let rectangulo = new Rectangulo(2, 5);
console.log(rectangulo.calcularArea());
