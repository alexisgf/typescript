"use strict";
function activar(quien, objeto = "Bomba", momento) {
    let mensaje;
    if (momento) {
        mensaje = `${quien} activo esta ${objeto} en este ${momento} `;
    }
    else {
        mensaje = `${quien} activo esta ${objeto}`;
    }
    console.log(mensaje);
}
activar("Alexis", "Party", "Instante");
