
interface Xmen {
    nombre:string,
    poder:string
}



function enviarMision(xmen:Xmen) {
    console.log("Enviando a: " + xmen.nombre);
}

function enviarCuartel(xmen: Xmen) {
    console.log("Enviando al Cuartel a: " + xmen.nombre);
}

let wolverin:Xmen = {
    nombre: "Wolverine",
    poder: "Regeneracion"
}

enviarMision(wolverin);
enviarCuartel(wolverin);