function activar(quien:string, objeto:string = "Bomba", momento?:string) {

    let mensaje:string;

    if(momento) {
        mensaje = `${ quien } activo esta ${ objeto } en este ${ momento } `;
    }

    else {
        mensaje = `${ quien } activo esta ${ objeto }`;
    }
    

    console.log(mensaje);
}

activar("Alexis", "Party" , "Instante");